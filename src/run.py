#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
from recdeleter import *
from ConfigParser import ConfigParser  # ver. < 3.0
config = ConfigParser()
# parse existing file
config.read('../configs/settings.ini')


paths = json.loads(config.get('Folders', 'constPaths'))
dirs = json.loads(config.get('Folders', 'lists'))
settings = {
    "logging": config.getboolean('Logger', 'NeedLog'),
    "OnlyLoging":config.getboolean('Debug', 'IsDebug')
}
print settings
replace = False
if len(paths)>0:
  replace = True

for idx, dir in enumerate(dirs):
  if replace:
    folder = paths[idx].replace("_FOLDER_CODE_",dir)
  else:
    folder = dir
  print "seeing folder: %s"%folder
  deleter(folder,True,settings)