#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
from datetime import *

def delete(path,settings):
    if settings["logging"]:
       now = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
       f = open('../logs/%s.log'%now, 'a')
       f.write("%s need deleted\n" % path)
       f.close()
    if settings["OnlyLoging"] is False:
        os.rmdir(path)

def deleter(kat, P = True,settings={}):
	if os.path.isdir(kat):
		if len(os.listdir(kat)) > 0:
			for x in os.listdir(kat):
				if os.path.isdir(os.path.join(kat,x)):
					if len(os.listdir(os.path.join(kat,x))) == 0: delete(os.path.join(kat,x),settings)
					else: deleter(os.path.join(kat,x),True,settings)
			if P: deleter(kat, False,settings)
		else: delete(kat,settings)